<?php

namespace GetNoticed\StoreNotice\Helper;

use Magento\Framework;
use Magento\Store\Model\ScopeInterface;

class ConfigHelper
    extends \GetNoticed\StoreNotice\Helper\Config\AbstractConfigHelper
{

    const XML_PATH_ENABLED = 'getnoticed_storenotice/settings/enable';

    const XML_PATH_ENABLED_MODAL = 'getnoticed_storenotice/settings/enable_modal';

    const XML_PATH_ENABLED_INLINE = 'getnoticed_storenotice/settings/enable_inline';

    public function isEnabled(): bool
    {
        return $this->isSetFlag(self::XML_PATH_ENABLED);
    }

    public function showModal(): bool
    {
        if ($this->isEnabled() !== true) {
            return false;
        }

        if ($this->getTitle() === '' || $this->getContent() === '') {
            return false;
        }

        return $this->isSetFlag(self::XML_PATH_ENABLED_MODAL);
    }

    public function showInline(): bool
    {
        if ($this->isEnabled() !== true) {
            return false;
        }

        if ($this->getTitle() === '' || $this->getContent() === '') {
            return false;
        }

        return $this->isSetFlag(self::XML_PATH_ENABLED_INLINE);
    }

    public function getTitle(): string
    {
        return $this->getStoreConfigValue('getnoticed_storenotice/settings/title');
    }

    public function getContent(): string
    {
        return $this->getStoreConfigValue('getnoticed_storenotice/settings/content');
    }

    public function getDisplayFromDate(): \DateTime
    {
        return $this->getDateFromConfig(
            'getnoticed_storenotice/settings/display_from_date',
            new \DateTime('1970-01-01')
        );
    }

    public function getDisplayUntilDate(): \DateTime
    {
        return $this->getDateFromConfig(
            'getnoticed_storenotice/settings/display_until_date',
            new \DateTime('+1 year')
        );
    }

    private function getDateFromConfig(string $path, \DateTime $default): \DateTime
    {
        $dateFrom = $this->getStoreConfigValue($path);

        if ($dateFrom === null) {
            return $default;
        }

        return new \DateTime($dateFrom);
    }

    public function getModalConfiguration(): array
    {
        return [
            'title'            => $this->getTitle(),
            'content'          => $this->getContent(),
            'displayFromDate'  => [
                'd' => $this->getDisplayFromDate()->format('d'),
                'm' => ((int)$this->getDisplayFromDate()->format('m')) - 1,
                'y' => $this->getDisplayFromDate()->format('Y')
            ],
            'displayUntilDate' => [
                'd' => $this->getDisplayUntilDate()->format('d'),
                'm' => ((int)$this->getDisplayUntilDate()->format('m')) - 1,
                'y' => $this->getDisplayUntilDate()->format('Y')
            ]
        ];
    }

    public function getStoreConfigValue(string $path) {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        ) ?: '';
    }

    public function isSetFlag(string $path) {
        return $this->scopeConfig->isSetFlag(
            $path,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

}
