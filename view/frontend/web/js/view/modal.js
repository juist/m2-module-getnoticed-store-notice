define([
    'jquery',
    'uiComponent',
    'ko',
    'mage/cookies'
], function ($, Component, ko) {
    'use strict';

    return Component.extend({
        showInline: ko.observable(false),
        title: ko.observable(''),
        content: ko.observable(''),

        initialize: function () {
            // Initialize
            var self = this;
            self._super();

            if (self.withinDateWindow() === true) {
                // Set modal data
                self.showInline(true);
                self.title(self.cfg.title);
                self.content(self.cfg.content);
            }
        },

        withinDateWindow: function () {
            var self = this;
            var displayFromDate = new Date(self.cfg.displayFromDate.y, self.cfg.displayFromDate.m, self.cfg.displayFromDate.d, 0, 0, 0, 0);
            var displayUntilDate = new Date(self.cfg.displayUntilDate.y, self.cfg.displayUntilDate.m, self.cfg.displayUntilDate.d, 0, 0, 0, 0);
            var currentDate = new Date();
            currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);

            return displayFromDate.getTime() < currentDate.getTime() && displayUntilDate.getTime() > currentDate.getTime();
        }
    });
});