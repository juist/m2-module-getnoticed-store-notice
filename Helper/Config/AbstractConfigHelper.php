<?php

namespace GetNoticed\StoreNotice\Helper\Config;

use Magento\Framework;
use Magento\Store;

abstract class AbstractConfigHelper
    extends Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

}
