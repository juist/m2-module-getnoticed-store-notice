<?php

namespace GetNoticed\StoreNotice\Block;

use Magento\Framework;
use GetNoticed\StoreNotice;

class Modal
    extends Framework\View\Element\Template
{

    /**
     * @var StoreNotice\Helper\ConfigHelper
     */
    protected $configHelper;

    public function __construct(
        StoreNotice\Helper\ConfigHelper $configHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->configHelper = $configHelper;

        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        return $this->configHelper->showModal() === true ? parent::_toHtml() : '';
    }

}