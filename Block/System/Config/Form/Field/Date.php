<?php

namespace GetNoticed\StoreNotice\Block\System\Config\Form\Field;

use Magento\Framework;
use Magento\Config;

class Date
    extends Config\Block\System\Config\Form\Field
{

    /**
     * @param Framework\Data\Form\Element\AbstractElement $element
     */
    public function render(
        \Magento\Framework\Data\Form\Element\AbstractElement $element
    ) {
        $element->setDateFormat(\Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT);
        $element->setTimeFormat(null);

        return parent::render($element);
    }

}