<?php

namespace GetNoticed\StoreNotice\Block\System\Config\Form\Field;

use Magento\Config;
use Magento\Backend;
use Magento\Cms;

class Wysiwyg
    extends Config\Block\System\Config\Form\Field
{

    /**
     * @var Cms\Model\Wysiwyg\Config
     */
    protected $wysiwygConfig;

    public function __construct(
        Cms\Model\Wysiwyg\Config $wysiwygConfig,
        Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->wysiwygConfig = $wysiwygConfig;

        parent::__construct($context, $data);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->setWysiwig(true);
        $element->setConfig($this->wysiwygConfig->getConfig($element));

        return parent::_getElementHtml($element);
    }

}